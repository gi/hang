import random as r
import copy
import logging
import sys

CHR_CIRCULAR = ')'
CHR_LINEAR = '|'
ORIENT_POSITIVE = '+'
ORIENT_NEGATIVE = '-'
EXTREMITY_TAIL = 't'
EXTREMITY_HEAD = 'h'
EXTREMITY_TELOMERE = '$'
CAP_ID='cap00'


SELFEDGE = 'self'
ADJACENCY = 'adj'
EXTREMITYEDGE = 'ext'
ADJACENCY_C = 'cadj'

#TODO: FIX THIS STUPID LOGGING SITUATION

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)
st = logging.StreamHandler(sys.stderr)
st.setLevel(logging.INFO)
st.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
LOG.addHandler(st)

class Simple_Id_Generator:
    last = 0
    def get_new(self):
        self.last+=1
        return self.last

MARKER_ID_GEN = Simple_Id_Generator()

dd = lambda x: '' if x==ORIENT_POSITIVE else x
class Marker:
    def __init__(self, gene, mid, direction=ORIENT_POSITIVE):
        self.gene = gene
        self.mid = mid
        self.direction = direction
    def __str__(self):
        return dd(self.direction) +str(self.gene)
    def get_extremities(self,directionless=False):
        exts = [((self.mid),EXTREMITY_TAIL), ((self.mid),EXTREMITY_HEAD)]
        if self.direction == ORIENT_POSITIVE or directionless:
            return exts
        else:
            return exts[::-1]


def are_same_adj(a,b):
    return a[0] == b[0] or a[1] == b[0] or a[0] == b[1] or a[1] == b[1]


def add_adjacencies_c(ctype,markers, adj):
    '''
    Add the adjacencies of a chromosome to the adjacency list passed.
    
    ctype: Chromosome type
    markers: Gene order string of the chromosome
    adj: List of adjacencies to append to
    '''
    last = markers[0]
    for m in markers[1::]:
        y = m.get_extremities()[0]
        x = last.get_extremities()[1]
        adj[x] = y
        adj[y] = x
        last = m
    e1 = markers[0].get_extremities()[0]
    e2 = markers[-1].get_extremities()[1]
    if ctype == CHR_LINEAR:
        adj[e1] = (None, EXTREMITY_TELOMERE)
        adj[e2] = (None, EXTREMITY_TELOMERE)
    else:
        adj[e1] = e2
        adj[e2] = e1

        

def get_adjacencies(chromosomes):
    adj = {}
    for chrom in chromosomes:
        add_adjacencies_c(chrom[0],chrom[1], adj)
    return adj

def get_marker_dict(chromosomes):
    '''
    Obtain a dictionary mapping marker ids to their family.
    '''
    md = {}
    for _,ms in chromosomes:
        for m in ms:
            md[m.mid] = m.gene
    return md
    
def get_familiy_dict(chromosomes):
    '''
    Obtain a dictionary mapping families to lists of markers with that family.
    '''
    fams = {}
    for _, ms in chromosomes:
        for m in ms:
            if not m.gene in fams:
                fams[m.gene] = []
            fams[m.gene].append(m)
    return fams

class DCJ_Genome:
    '''
    Representation of a genome that is able to perform pseudo-random DCJs and deletions on itself.
    '''
    def __init__(self, adj):
        self.adj = adj
        self.telomeres = set()
        for k,x in list(self.adj.items()):
            if x[1] == EXTREMITY_TELOMERE:
                del adj[k]
                self.telomeres.add(k)
    def __str__(self):
        return str(self.adj)+'||'+str(self.telomeres)
    def rdcj_a_a(self):
        '''
        Perform a pseudo-random DCJ between two adjacencies. 
        '''
        if len(self.adj.keys()) <= 2:
            return False
        a = (None, None)
        b = (None, None)
        while are_same_adj(a,b):
            k1 = r.choice(list(self.adj.keys()))
            k2 = r.choice(list(self.adj.keys()))
            a = [k1,self.adj[k1]]
            b = [k2,self.adj[k2]]
        r.shuffle(a)
        r.shuffle(b)
        self.adj[a[0]] = b[0]
        self.adj[b[0]] = a[0]
        self.adj[a[1]] = b[1]
        self.adj[b[1]] = a[1]
        LOG.info('DCJ between adjacencies {} and {} lead to adjacencies {} and {}'.format(a,b, [a[0],b[0]],[a[1],b[1]]))
        return True
    def rdcj_a_t(self):
        '''
        Perform a pseudo-random DCJ on an adjacency and a telomere.
        '''
        if len(self.adj.keys()) == 0 or len(self.telomeres) ==0:
            return False
        k = r.choice(list(self.adj.keys()))
        a = [k, self.adj[k]]
        t = r.choice(list(self.telomeres))
        r.shuffle(a)
        self.adj[a[0]] = t
        self.adj[t] = a[0]
        del self.adj[a[1]]
        self.telomeres.discard(t)
        self.telomeres.add(a[1])
        LOG.info('DCJ between adjacency {} and telomere {} lead to adjacency {} and telomere {}'.format(a,t, [a[0],t],a[1]))
        return True
    def rdcj_a(self):
        '''
        Perform a pseudo-random SCJ cut on one adjacency.
        '''
        if len(self.adj.keys()) == 0:
            return False
        k = r.choice(list(self.adj.keys()))
        a = [k, self.adj[k]]
        del self.adj[a[0]]
        del self.adj[a[1]]
        self.telomeres.add(a[0])
        self.telomeres.add(a[1])
        LOG.info('DCJ Cut on between adjacency {} lead to telomeres {} and {}'.format(a,a[0],a[1]))
        return True
    def rdcj_t_t(self):
        '''
        Perform a pseudo-random SCJ join between two telomeres.
        '''
        if len(self.telomeres)  < 2:
            return False
        t1 = None
        t2 = None
        while t1 == t2:
            t1 = r.choice(list(self.telomeres))
            t2 = r.choice(list(self.telomeres))
        self.telomeres.discard(t1)
        self.telomeres.discard(t2)
        self.adj[t1] = t2
        self.adj[t2] = t1
        LOG.info('DCJ Join between telomeres {} and {} lead to adjacency {}'.format(t1, t2,[t1,t2]))
    def reconstruct_genome(self,marker_dict=None):
        '''
        Reconstruct the chromosomes of the genome.
        
        marker_dict: Dictionary mapping marker ids to their family
        returns : chromosomes of the reconstructed genome
        '''
        remaining_telos = copy.copy(self.telomeres)
        remaining_adjs = copy.copy(self.adj)
        chromosomes = []
        #reconstruct linear first
        while len(remaining_telos) > 0:
            chromosomes.append(reconstruct_lchromosome(remaining_telos, remaining_adjs,marker_dict, remaining_telos.pop()))
        while len(remaining_adjs) > 0:
            x = next(iter(remaining_adjs))
            chromosomes.append(reconstruct_cchromosome(remaining_adjs, marker_dict,x))
        return chromosomes
    def random_dcj(self, a_a_rate=None, a_t_rate=None, t_t_rate=None):
        '''
        Perform a pseudo-random DCJ with given rates for each possible event.
        These rates are overridden if it is not possible to perform the operation (i.e. lack of telomeres).
        '''
        rts = {}
        rts['a_a_rate'] = a_a_rate
        rts['a_t_rate'] = a_t_rate
        rts['t_t_rate'] = t_t_rate
        rts['a_rate'] = None
        ntel = len(self.telomeres)
        nadj = len(self.adj)
        na = nadj/2
        nt = ntel
        if ntel + nadj == 0:
            #stop if there is no genome to rearrange anymore
            LOG.warning('Deletions have depleted the genome fully.')
            return
        # fix rates if some operations are not possible
        total_default_rate = na*(na-1) + (2*na*nt) + 0.5*(nt*(nt-1)) + na
        if nadj == 0:
            rts['a_a_rate'] = 0
            rts['a_t_rate'] = 0
            rts['a_rate'] = 0
        if nadj == 1:
            rts['a_a_rate'] = 0
        if ntel == 0:
            rts['t_t_rate'] = 0
            rts['a_t_rate'] = 0
        if rts['a_a_rate'] == None:
            rts['a_a_rate'] = (na*(na-1))/total_default_rate
        if rts['a_t_rate'] == None:
            rts['a_t_rate'] = (2*na*nt)/total_default_rate
        if rts['t_t_rate'] == None:
            rts['t_t_rate'] = 0.5*(nt*(nt-1))/total_default_rate
        if rts['a_rate'] == None:
            rts['a_rate'] = na/total_default_rate
        #normalize
        total = 0
        for rate in rts.values():
            total+=rate
        for k, rate in rts.items():
            rts[k] = rate/total
        k = r.uniform(0,1)
        ceil = rts['a_a_rate']
        if k <= ceil:
            self.rdcj_a_a()
            return
        ceil+=rts['a_t_rate']
        if k <= ceil:
            self.rdcj_a_t()
            return
        ceil+=rts['t_t_rate']
        if k <= ceil:
            self.rdcj_t_t()
            return
        self.rdcj_a()

    def deletion_loop(self,curr_del, right_border, max_size):
        '''
        Delete a segment from the DCJ_Genome.
        
        curr_del: the vertex the deletion is currently at
        right_border: the vertex adjacent to the start of the deletion
        max_size: maximum size of the segment to be deleted (shortened if a telomere or right_border is reached)
        '''
        telo_reached = False
        round_reached = False
        for _ in range(max_size):
            old_del = curr_del
            if sibling_extremity(curr_del) == right_border:
                del self.adj[old_del]
                del self.adj[sibling_extremity(old_del)]
                round_reached = True
                break
            if sibling_extremity(old_del) in self.adj:
                curr_del = self.adj[sibling_extremity(old_del)]
                del self.adj[old_del]
                del self.adj[sibling_extremity(old_del)]
            else:
                del self.adj[old_del]
                self.telomeres.remove(sibling_extremity(old_del))
                telo_reached = True
                break
        return curr_del, round_reached, telo_reached
    def rdel_a(self,max_size):
        '''
        Perform a pseudo-random deletion starting in an adjacency.
        '''
        right_border = r.choice(list(self.adj.keys()))
        curr_del = self.adj[right_border]
        curr_del, round_reached, telo_reached = self.deletion_loop(curr_del, right_border, max_size)
        if round_reached:
            LOG.info('Deletion of circular chromosome containing {}.'.format(right_border))
            return
        if telo_reached:
            LOG.info('Deletion of segment near end of linear chromosome leading to telomere {}.'.format(right_border))
            del self.adj[right_border]
            self.telomeres.add(right_border)
        else:
            LOG.info('Deletion of segment of length {} creating adjacency {}.'.format(max_size,[right_border, curr_del]))
            self.adj[right_border] = curr_del
            self.adj[curr_del] = right_border
    def rdel_t(self, max_size):
        '''
        Perform a pseudo-random deletion starting at a telomere.
        '''
        curr_del = r.choice(list(self.telomeres))
        self.telomeres.remove(curr_del)
        if sibling_extremity(curr_del) in self.telomeres:
            self.telomeres.remove(sibling_extremity(curr_del))
            LOG.info('Deletion of segment of linear chromosome containing {}.'.format(curr_del))
            return
        old_del = curr_del
        curr_del = self.adj[sibling_extremity(curr_del)]
        del self.adj[sibling_extremity(old_del)]
        curr_del, round_reached, telo_reached = self.deletion_loop(curr_del, old_del, max_size-1)
        if telo_reached:
            LOG.info('Deletion of linear chromosome containing {}.'.format(old_del))
        else:
            LOG.info('Deletion of segment of length {} creating telomere {}.'.format(max_size,curr_del))
            del self.adj[curr_del]
            self.telomeres.add(curr_del)
    def random_deletion(self, max_size):
        '''
        Perform a pseudo-random deletion.
        '''
        if len(self.adj) + len(self.telomeres) == 0:
            LOG.warning('Deletions have depleted the genome.')
            return
        total = len(self.adj)+ len(self.telomeres)
        a_rate = (len(self.adj))/total
        if r.uniform(0,1) <= a_rate:
            self.rdel_a(max_size)
        else:
            self.rdel_t(max_size)
        
        
        
def get_fam(m,marker_dict):
    '''
    Wrapper functionality, returning the marker id as the family if no dict to determine the family was provided.
    '''
    if marker_dict == None:
        return m
    return marker_dict[m]

def reconstruct_cchromosome(adj, marker_dict, start_ext):
    '''
    Reconstruct a circular chromosome from a set of adjacencies.
    
    adj: dict of adjacencies. WARNING: this will be edited
    marker_dict: dictionary mapping marker ids to families
    start_ext: beginning extremity of the chromosome
    '''
    next_ext = sibling_extremity(start_ext)
    markers = []
    while next_ext in adj:
        m, x = next_ext
        if next_ext[1] == EXTREMITY_HEAD:
            markers.append(Marker(get_fam(m,marker_dict),m, direction=ORIENT_POSITIVE))
        else:
            markers.append(Marker(get_fam(m,marker_dict),m, direction=ORIENT_NEGATIVE))
        n = sibling_extremity(adj[next_ext])
        del adj[adj[next_ext]]
        del adj[next_ext]
        next_ext = n
    return (CHR_CIRCULAR, markers)

            
def reconstruct_lchromosome(telos, adj, marker_dict, start_telo):
    '''
    Reconstruct a linear chromosome from a set of adjacencies and telomeres.
    
    telos: set of telomeres. WARNING: this will be edited
    adj: dict of adjacencies. WARNING: this will be edited
    marker_dict: dictionary mapping marker ids to families
    start_telo: beginning telomere of the chromosome
    '''
    next_ext = sibling_extremity(start_telo)
    markers = []
    while next_ext in adj:
        m, x = next_ext
        if next_ext[1] == EXTREMITY_HEAD:
            markers.append(Marker(get_fam(m,marker_dict),m, direction=ORIENT_POSITIVE))
        else:
            markers.append(Marker(get_fam(m,marker_dict),m, direction=ORIENT_NEGATIVE))
        n = sibling_extremity(adj[next_ext])
        del adj[adj[next_ext]]
        del adj[next_ext]
        next_ext = n
    m, x = next_ext
    if next_ext[1] == EXTREMITY_HEAD:
        markers.append(Marker(get_fam(m,marker_dict),m, direction=ORIENT_POSITIVE))
    else:
        markers.append(Marker(get_fam(m,marker_dict),m, direction=ORIENT_NEGATIVE))
    telos.remove(next_ext)
    return (CHR_LINEAR, markers)


def rev(m):
    '''
    Change the direction on a marker.
    '''
    if m.direction == ORIENT_NEGATIVE:
        m.direction = ORIENT_POSITIVE
    else:
        m.direction = ORIENT_NEGATIVE
    return m

def reverse(markers):
    '''
    Perform an inversion on a gene order string.
    '''
    return [rev(m) for m in reversed(markers)]

def normalize_linear(markers,marker_ids=False):
    '''
    Obtain a canonized version of a linear gene order string.
    
    markers: gene order string
    marker_ids: use marker ids instead of families for canonization
    '''
    ret = copy.deepcopy(markers)
    fst = ret[0]
    last = ret[-1]
    if not marker_ids:
        if fst.gene > last.gene or (fst.gene == last.gene and fst.direction == ORIENT_NEGATIVE):
            return reverse(markers)
    else:
        if fst.mid > last.mid or (fst.mid == last.mid and fst.direction == ORIENT_NEGATIVE):
            return reverse(markers)
    return markers

def normalize_circular(markers,marker_ids=False):
    '''
    Obtain a canonized version of a circular gene order string.
    
    markers: gene order string
    marker_ids: use marker ids instead of families for canonization
    '''
    ret = copy.deepcopy(markers)
    if not marker_ids:
        i,_ = min(enumerate(ret), key=lambda tp: tp[1].gene)
    else:
        i,_ = min(enumerate(ret), key=lambda tp: tp[1].mid)
    if ret[i].direction == ORIENT_POSITIVE:
        ret_ = ret[i:]+ret[:i]
        return ret_
    else:
        i = (i+1)%len(ret)
        ret_ = ret[i:]+ret[:i]
        ret_ = reverse(ret_)
        return ret_

def normalize_chromosomes(chromosomes,marker_ids=False):
    '''
    Canonize multiple chromosomes.
    '''
    chrs = []
    for chrtype, markers in chromosomes:
        if chrtype == CHR_LINEAR:
            markers_ = normalize_linear(markers,marker_ids = marker_ids)
        else:
            markers_ = normalize_circular(markers,marker_ids = marker_ids)
        chrs.append((chrtype,markers_))
    return chrs

def print_chromosomes(chromosomes,file=sys.stdout,marker_ids = False):
    '''
    Print a collection of chromosomes in unimog-format.
    '''
    for chrtype, markers in chromosomes:
        for marker in markers:
            if not marker_ids:
                print(marker, end=' ',file=file)
            else:
                print('{}{}'.format(dd(marker.direction), marker.mid),end=' ',file=file)
        print(chrtype,file=file)



def complement_x(x):
    if x == EXTREMITY_HEAD:
        return EXTREMITY_TAIL
    return EXTREMITY_HEAD

def sibling_extremity(e):
    return (e[0], complement_x(e[1]))
