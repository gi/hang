#!/usr/bin/python3

from ilp_util import *
from argparse import ArgumentParser
import argparse as ap
import sys

def c01(sng,idxe):
    cons = []
    idgen = Simple_Id_Generator()
    for e in [idxe[(x,y,key)] for (x,y,key,etype) in sng.edges(keys = True, data='etype') if etype==ADJACENCY]:
        cons.append('c01.{cid}: x{sep}{e} = 1'.format(cid=idgen.get_new(),sep=SEPARATOR,e=e))
    return cons

def c02(sng,idxe,cappingfree=True):
    cons = []
    c07gen = Simple_Id_Generator()
    for v,ntype in sng.nodes(data='ntype'):
        if ntype==CAP and cappingfree:
            continue
        c07list = []
        for x,y,key,etype in sng.edges(v,keys=True,data='etype'):
            term = 'x{sep}{e}'.format(sep=S,e=idxe[(x,y,key)])
            if etype==ADJACENCY:
                continue
            c07list.append(term)
        cons.append('c02.{cid}: {sum} = 1'.format(cid=c07gen.get_new(),sum=' + '.join(c07list)))
    return cons

def c03(sng,idxe):
    cons = []
    idgen = Simple_Id_Generator()
    for e1,e2 in [(idxe[ex],idxe[ey]) for ex,ey in get_sibling_edges(sng)]:
        cons.append('c03.{cid}: x{sep}{e1} - x{sep}{e2} = 0'.format(cid=idgen.get_new(),sep=S,e1=e1,e2=e2))
    return cons

def c04(sng,idxv,idxe):
    cons = []
    idgen = Simple_Id_Generator()
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        u = idxv[x]
        v = idxv[y]
        if not etype == SELFEDGE:
            cons.append('c04.{cid}: y{sep}{u} - y{sep}{v} + {u} x{sep}{e} <= {u}'.format(sep=S,cid=idgen.get_new(),u=u,v=v,e=e))
            cons.append('c04.{cid}: y{sep}{u} - y{sep}{v} + {u} x{sep}{e} <= {u}'.format(sep=S,cid=idgen.get_new(),u=v,v=u,e=e))
        else:
            cons.append('c04.{cid}: y{sep}{u} + {u} x{sep}{e} <= {u}'.format(sep=S,cid=idgen.get_new(),u=u,e=e))
            cons.append('c04.{cid}: y{sep}{u} + {u} x{sep}{e} <= {u}'.format(sep=S,cid=idgen.get_new(),u=v,e=e))
    return cons

def c05(sng,idxv):
    cons = []
    idgen = Simple_Id_Generator()
    for x in sng.nodes:
        v = idxv[x]
        cons.append('c05.{cid}: {v} z{sep}{v} - y{sep}{v} <= 0'.format(cid=idgen.get_new(),v=v,sep=S))
    return cons

def c06(fams,idxe,sng,mtype='mm'):
    cons = []
    idgen = Simple_Id_Generator()
    for f,ms in fams.items():
        sxvars = []
        for m in ms:
            x,y = m.get_extremities()
            slfedges = [idxe[(x,y,key)] for key, data in sng[x][y].items() if data['etype']==SELFEDGE]
            if len(slfedges) != 1:
                raise AssertionError('{} indel edges for marker {}'.format(len(slfedges),m))
            se = slfedges[0]
            sxvars.append('x{}{}'.format(S,se))
        if mtype=='mm':
            cons.append('cmm.{}: {} <= {}'.format(idgen.get_new(),' + '.join(sxvars),len(ms)%2))
        elif mtype == 'em':
            cons.append('cem.{}: {} = {}'.format(idgen.get_new(),' + '.join(sxvars), max(len(ms)-2,len(ms)%2)))
        elif mtype == 'im':
            cons.append('cim.{}: {} <= {}'.format(idgen.get_new(),' + '.join(sxvars), max(len(ms)-2,len(ms)%2)))
    return cons


def c07(sng,idxv):
    cons = []
    idgen = Simple_Id_Generator()
    for v,ntype in sng.nodes(data='ntype'):
        i = idxv[v]
        if ntype==CAP:
            cons.append('c07.{cid}: b{sep}{i} = 0'.format(sep=S,i=i,cid=idgen.get_new()))
    return cons

def c080910(sng,idxe,idxv,capping=False):
    cons = []
    idgen = Simple_Id_Generator()
    idgen2 = Simple_Id_Generator()
    idgen3 = Simple_Id_Generator()
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        u = idxv[x]
        v = idxv[y]
        if etype == SELFEDGE:
            for i in [u,v]:
                cons.append("c08.{cid}: b{sep}{i} + x{sep}{e} <= 1".format(sep=S,e=e,i=i,cid=idgen.get_new()))
        elif etype==EXTREMITYEDGE:
            cons.append("c09.{cid}: b{sep}{v} + b{sep}{u} + x{sep}{e} <= 2".format(sep=S,cid=idgen2.get_new(),e=e,v=v,u=u))
            cons.append("c09.{cid}: b{sep}{v} + b{sep}{u} - x{sep}{e} >= 0".format(sep=S,cid=idgen2.get_new(),e=e,v=v,u=u))
        elif etype == ADJACENCY:
            for i,j in [(u,v),(v,u)]:
                if not capping:
                    cons.append("c10.{cid}: b{sep}{i} - b{sep}{j} + risi{sep}{e} + rtsi{sep}{e} + rtst{sep}{e} + rcs{sep}{e} >= 0". format(cid=idgen3.get_new(),sep=S,j=j,i=i,e=e))
                else:
                        cons.append("c10.{cid}: b{sep}{i} - b{sep}{j} + risi{sep}{e} + rcs{sep}{e} >= 0". format(cid=idgen3.get_new(),sep=S,j=j,i=i,e=e)) 
    return cons

def c11(sng,idxv):
    cons = []
    idgen = Simple_Id_Generator()
    for x,ntype in sng.nodes(data='ntype'):
        v = idxv[x]
        if ntype==CAP:
            cons.append('c11.{cid}: a{sep}{v} = 0'.format(cid=idgen.get_new(),sep=S,v=v))
    return cons

def c121314(sng,idxe,idxv):
    cons = []
    idgen = Simple_Id_Generator()
    idgen2 = Simple_Id_Generator()
    idgen3 = Simple_Id_Generator()
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        i,j = idxv[x],idxv[y]
        if etype==SELFEDGE:
            for u in [i,j]:
                cons.append('c12.{cid}: x{sep}{e} - a{sep}{u} <= 0'.format(cid=idgen.get_new(),sep=S,u=u,e=e))
        elif etype==EXTREMITYEDGE:
            for u,v in[(i,j),(j,i)]:
                cons.append('c13.{cid}: a{sep}{v} - a{sep}{u} + x{sep}{e} <= 1'.format(cid=idgen2.get_new(),sep=S,v=v,u=u,e=e))
        elif etype==ADJACENCY:
            for u,v in[(i,j),(j,i)]:
                cons.append('c14.{cid}: a{sep}{u} - a{sep}{v} + rtsi{sep}{e} + rtoi{sep}{e} >= 0'.format(cid=idgen3.get_new(),sep=S,u=u,v=v,e=e))
    return cons

def c15toc19(sng,idxe,idxv,capping=False):
    cons = []
    gens = {}
    for i in range(15,20):
        gens[i]=Simple_Id_Generator()
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        u,v = idxv[x],idxv[y]
        if etype!=ADJACENCY:
            continue
        if not capping:
            cons.append('c15.{cid}: rco{sep}{e} + rcs{sep}{e} + rtst{sep}{e} - z{sep}{u} - z{sep}{v} <= 0'.format(cid=gens[15].get_new(),sep=S,e=e,u=u,v=v))
            for rvar in ['rtst','rtsi']:
                cons.append('c16.{cid}: {rvar}{sep}{e} - b{sep}{u} - b{sep}{v} <= 0'.format(cid=gens[16].get_new(),sep=S,e=e,u=u,v=v,rvar=rvar))
                cons.append('c17.{cid}: {rvar}{sep}{e} + b{sep}{u} + b{sep}{v} <= 2'.format(cid=gens[17].get_new(),sep=S,e=e,u=u,v=v,rvar=rvar))
            if CAP not in [sng.nodes(data=True)[j]['ntype'] for j in [x,y]]:
                for rvar in ['rtst','rtsi','rtoi']:
                    cons.append('c18.{cid}: {rvar}{sep}{e} = 0'.format(cid=gens[18].get_new(),sep=S,e=e,rvar=rvar))
        else:
            cons.append('c15.{cid}: rco{sep}{e} + rcs{sep}{e} - z{sep}{u} - z{sep}{v} <= 0'.format(cid=gens[15].get_new(),sep=S,e=e,u=u,v=v))
        ides = []
        for z in [x,y]:
            if sng.nodes(data=True)[z]['ntype'] == CAP:
                continue
            idedges = [(z,w,k) for w in sng[z] for k in sng[z][w] if sng[z][w][k]['etype']==SELFEDGE]
            if len(idedges) != 1:
                raise AssertionError("{n} indel edges adjacent to node {z}!".format(n=len(idedges),z=z))
            ides.append(idxe[idedges[0]])
        for rvar in ['risi']:
            xvars = ' - '.join(['x{sep}{ide}'.format(sep=S,ide=ide) for ide in ides])
            if len(xvars) == 0:
                'c19.{cid}: {rvar}{sep}{e} = 0'.format(cid=gens[18].get_new(),rvar=rvar,sep=S,e=e)
            else:
                cons.append('c19.{cid}: {rvar}{sep}{e} - {xvars} <= 0'.format(cid=gens[18].get_new(),rvar=rvar,sep=S,e=e,xvars=xvars))
    return cons


def c20toc31(sng,idxe,capping=False):
    cons = []
    adje = []
    exte = []
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        if etype==ADJACENCY:
            adje.append(e)
        elif etype==EXTREMITYEDGE:
            exte.append(e)
    cons.append('c20: {sum} - n = 0'.format(sum=' + '.join(['0.5 x{sep}{e}'.format(sep=S,e=e) for e in exte])))
    cons.append('c21: {sum} - c = 0'.format(sum=' + '.join(['rco{sep}{e}'.format(sep=S,e=e) for e in adje])))
    if not capping:
        cons.append('c22: pisi + pti - ptst + d - 2 q <= 0')
    else:
        cons.append('c22: pisi - 2 q <= 0')
    cons.append('c23: {sum} - pisi = 0'.format(sum=' + '.join(['risi{sep}{e}'.format(sep=S,e=e) for e in adje])))
    if not capping:
        cons.append('c24: {sum} - ptsi = 0'.format(sum=' + '.join(['rtsi{sep}{e}'.format(sep=S,e=e) for e in adje])))
        cons.append('c25: {sum} - ptoi = 0'.format(sum=' + '.join(['rtoi{sep}{e}'.format(sep=S,e=e) for e in adje])))
        cons.append('c26: pti - ptsi >= 0')
        cons.append('c27: pti - ptoi >= 0')
        cons.append('c28: {sum} - ptst = 0'.format(sum=' + '.join(['rtst{sep}{e}'.format(sep=S,e=e) for e in adje])))
        cons.append('c29: pisi - 2 T - O = 0')
        cons.append('c30: 2 pti - ptsi - ptoi - NE >= 0')
        cons.append('c31: d - O + NE >= 0')
    return cons

BND_LOW = 'lo'
BND_HIGH = 'hi'

def vars(sng,idxv,idxe,capping=False):
    binaries = []
    generals = {}
    #vertex variables
    for x in sng.nodes():
        v = idxv[x]
        y = 'y{sep}{v}'.format(sep=S,v=v)
        generals[y]={}
        generals[y][BND_HIGH]=v
        generals[y][BND_LOW]=0
        binaries.append('z{sep}{v}'.format(sep=S,v=v))
        if not capping:
            binaries.append('a{sep}{v}'.format(sep=S,v=v))
        binaries.append('b{sep}{v}'.format(sep=S,v=v))
    #edge variables
    ALL_RVARS = ['rtst','rtoi','rtsi','risi','rco','rcs'] if not capping else ['rco','rcs','risi']
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        binaries.append('x{sep}{e}'.format(sep=S,e=e))
        if etype==ADJACENCY:
            for rvar in ALL_RVARS:
                binaries.append('{rvar}{sep}{e}'.format(rvar=rvar,sep=S,e=e))
    generals['n'] = {BND_LOW : 0}
    ALL_SUMVARS =  ['c','pisi','s']
    for sumvar in ALL_SUMVARS:
        generals[sumvar] = {BND_LOW : 0}
    generals['q'] = {}
    generals['T'] = {}
    if not capping:
        binaries.append('O')
        binaries.append('NE')
        binaries.append('d')
    return binaries,generals


def objective():
    return 'n - c + q + s'

def c32(sng,chromosomes,idxe):
    idgen = Simple_Id_Generator()
    cons = []
    svars = []
    for ctype, markers in chromosomes:
        if ctype == CHR_LINEAR:
            continue
        cid = idgen.get_new()
        summands = []
        for m in markers:
            x,y = tuple(m.get_extremities())
            if not sng.has_edge(x,y):
                continue
            es = [idxe[(x,y,eid)] for eid,data in sng[x][y].items() if data['etype']==SELFEDGE]
            if len(es) > 1:
                raise AssertionError('Too many selfedges between {} and {}'.format(x,y))
            if len(es) < 1:
                continue
            e = es[0]
            summands.append('x{sep}{e}'.format(sep=S,e=e))
        if len(summands) < len(markers):
            continue
        cons.append('c06.{cid}: {sum} - s{sep}{cid} <= {l}'.format(cid=cid,sum=' + '.join(summands),sep=S,l=len(markers)-1))
        svars.append('s{sep}{cid}'.format(sep=S,cid=cid))
    if len(cons) == 0:
        cons.append('c33: s = 0')
    else:
        cons.append('c33: {sum} - s = 0'.format(sum=' + '.join(svars)))
    return cons, svars

def c34(sng,idxe,idxv):
    cons = []
    idgen = Simple_Id_Generator()
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        if etype==ADJACENCY:
            u,v = idxv[x],idxv[y]
            cons.append('c34.{cid}: 2 rco{sep}{e} - a{sep}{u} - a{sep}{v} <= 0'.format(cid=idgen.get_new(),sep=S,e=e,u=u,v=v))
    return cons

def c35(sng,idxe,idxv):
    cons = []
    idgen = Simple_Id_Generator()
    for x,y,key,etype in sng.edges(keys=True,data='etype'):
        e = idxe[(x,y,key)]
        if etype!=ADJACENCY:
            continue
        u,v = idxv[x],idxv[y]
        for rvar in ['risi','rtsi','rtoi']:
            cons.append('c35.{cid}: y{sep}{u} + y{sep}{v} + {lsum} {rvar}{sep}{e} <= {lsum}'.format(cid=idgen.get_new(),sep=S,e=e,u=u,v=v,lsum=u+v,rvar=rvar))
    return cons
    
def main():
    parser = ArgumentParser("Generate a capping-free ILP to halve natural Genomes.")
    parser.add_argument("input",help="Unimog file of the input genome.")
    parser.add_argument('--matching-model',choices=['mm','im','em'],default='mm')
    parser.add_argument("--outfile",type=ap.FileType('w'),default=sys.stdout)
    parser.add_argument("--capping",action='store_true')
    args = parser.parse_args()
    #print(args.capping)
    gnms = readGenomes(args.input)
    if len(gnms) < 1:
        LOG.info('No genomes in file! Exiting...')
        return
    if len(gnms) > 1:
        LOG.warning('Will ignore other genomes except first.')
    gnm = gnms[0]
    chromosomes = gnm[1]
    sng, ces, md = build_graph(gnm,closing_style='bws')

    create_capping(sng,light=not args.capping)
    idxv = generate_vertex_indices(sng,start_index=1,caps_low=not args.capping)
    idxe = generate_edge_indices(sng,start_index=1)
    cons = []
    cons.extend(c01(sng=sng,idxe=idxe))
    cons.extend(c02(sng=sng,idxe=idxe,cappingfree=not args.capping))
    cons.extend(c03(sng=sng,idxe=idxe))
    cons.extend(c04(sng=sng,idxv=idxv,idxe=idxe))
    cons.extend(c05(sng=sng,idxv=idxv))
    fams = get_familiy_dict(chromosomes)
    cons.extend(c06(fams=fams,sng=sng,mtype=args.matching_model,idxe=idxe))
    if not args.capping:
        cons.extend(c07(sng=sng,idxv=idxv))
    cons.extend(c080910(sng=sng,idxe=idxe,idxv=idxv,capping=args.capping))
    if not args.capping:
        cons.extend(c11(sng=sng,idxv=idxv))
    if not args.capping:
        cons.extend(c121314(sng=sng,idxe=idxe,idxv=idxv))
    cons.extend(c15toc19(sng=sng,idxe=idxe,idxv=idxv,capping=args.capping))
    cons.extend(c20toc31(sng=sng,idxe=idxe,capping=args.capping))
    singleton_cons, singleton_vars = c32(sng=sng,chromosomes=chromosomes,idxe=idxe)
    cons.extend(singleton_cons)
    if not args.capping:
        cons.extend(c34(sng=sng,idxe=idxe,idxv=idxv))
    cons.extend(c35(sng=sng,idxe=idxe,idxv=idxv))
    binaries,generals = vars(sng,idxv=idxv,idxe=idxe,capping=args.capping)
    binaries.extend(singleton_vars)
    f=args.outfile
    print('Minimize',file=f)
    print(' {}'.format(objective()),file=f)
    print('Subject To',file=f)
    for c in cons:
        print(' {}'.format(c),file=f)
    print('Bounds',file=f)
    for gvar, bnds in generals.items():
        upper = bnds.get(BND_HIGH,'Inf')
        lower = bnds.get(BND_LOW,'-Inf')
        print(' {lo} <= {gvar} <= {hi}'.format(lo=lower,hi=upper,gvar=gvar),file=f)
    print('Generals',file=f)
    for gvar in generals:
        print(' {}'.format(gvar),file=f)
    print('Binaries',file=f)
    for bvar in binaries:
        print(' {}'.format(bvar),file=f)
    print('End',file=f)




main()