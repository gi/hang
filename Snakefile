import itertools

configfile: 'config.yaml'

OLD_SCRIPTS = 'old'
NUM_SAMPLES = config['num_samples']
UNIMOG = config['unimog_cmd']


ILP_THREADS = config['ilp_threads']
ILP_TIME = config['ilp_time']

MODE = config['mode']



RAW_ILPS = config['ilps']
ILP_MODS = {}
#ILP_MODS['naive'] = config['naive_mods'] if 'naive_mods' in config else ['']
#ILP_MODS['plain'] = config['plain_mods'] if 'plain_mods' in config else ['']
#ILP_MODS['condensed'] = config['condensed_mods'] if 'condensed_mods' in config else ['']
ILP_MODS['journal'] = config['journal_mods'] if 'journal_mods' in config else ['']

ALL_ILPS = ['{}_{}'.format(name,mods) for name in RAW_ILPS for mods in ILP_MODS[name]]

MOD_MAP = {'C' : '--canonize-label-start', 'S' : '--ilp-specific-optimization', 'T' : '--no-t-rounding', 'M' : '--matching-model mm', 'E' : '--matching-model em', 'I' : '--matching-model im','O' : '--capping'}




SIMULATION_PARAMS = ['ops','markers','linears','duprate','dupzipf','delrate','insrate','idzipf']

simconfig = {}
for p in SIMULATION_PARAMS:
	if isinstance(config[p],list):
		simconfig[p] = config[p].copy()
	else:
		simconfig[p] = [config[p]]

groups = config['groups'].copy()

for p in SIMULATION_PARAMS:
	grouped = False
	for g in groups:
		if p in g:
			grouped=True
	if not grouped:
		groups.append([p])

GROUPED_PARAMS = [list(zip(*[[(name,e) for e in simconfig[name]] for name in group])) for group in groups]


#fs = 'bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}'

fs = 'ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}'
ALL_SIMULATIONS = []
for group_group in itertools.product(*GROUPED_PARAMS):
	td = {}
	for group in group_group:
		for name,val in group:
			td[name]=val
	ALL_SIMULATIONS.append(fs.format(ops=td['ops'],markers=td['markers'],linears=td['linears'],delrate=td['delrate'],insrate=td['insrate'],duprate=td['duprate'],idzipf=td['idzipf'],dupzipf=td['dupzipf']))


    
rule all_summarize:
    input:        
        expand('summary/%s/{simulation}/sample{n}.txt'%MODE,simulation=ALL_SIMULATIONS,n=range(1,NUM_SAMPLES+1))
    output:
        '%s_summary.txt'%MODE
    shell:
        'cat {input} > {output}'


rule all_simulation:
    input:
        expand('genomes/%s/simulated/{simulation}/sample{n}.txt'%MODE,simulation=ALL_SIMULATIONS,n=range(1,NUM_SAMPLES+1))


'''
rule simulate_singular:
    output:
        task='genomes/singular/simulated/bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}/sample{n}.txt',
        true='genomes/singular/simulated_truth_from_singular/bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}/sample{n}.txt'
    log:
        'genomes/singular/simulated/bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}/sample{n}.log'
    shell:
        './simulate.py --bounds {wildcards.low} {wildcards.up} --singular -nm {wildcards.markers} --del-rate {wildcards.delrate} --bounds-indel {wildcards.idlow} {wildcards.idup} --num-linears {wildcards.linears} --num-circulars {wildcards.circulars} --write-true-matching {output.true} > {output.task} 2> {log}'

rule simulate_natural:
    output:
        task='genomes/natural/simulated/bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}/sample{n}.txt',
        true='genomes/singular/simulated_truth_from_natural/bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}/sample{n}.txt',
    log:
        'genomes/natural/simulated/bnds{low}_{up}_markers{markers}_singular_theta{theta}_linears{linears}_circulars{circulars}_delrate{delrate}_indelbnds{idlow}_{idup}/sample{n}.log'
    shell:
        './simulate.py --bounds {wildcards.low} {wildcards.up} --num-families {wildcards.markers} -nm {wildcards.markers} --del-rate {wildcards.delrate} --bounds-indel {wildcards.idlow} {wildcards.idup} --num-linears {wildcards.linears} --num-circulars {wildcards.circulars} --theta {wildcards.theta} --write-true-matching {output.true}  > {output.task} 2> {log}'
'''


rule compile_simulator:
    output:
        temp('simulate')
    shell:
        './compile.sh'

rule simulate_singular:
    input:
        'simulate'
    output:
        task='genomes/singular/simulated/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/sample{n}.txt',
    log:
        'genomes/singular/simulated/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/sample{n}.log'
    shell:
        './simulate --genes {wildcards.markers} --nops {wildcards.ops} --nchrms {wildcards.linears} --dup-rate 0.0 --dup-size-zipf {wildcards.dupzipf} --del-rate {wildcards.delrate} --ins-rate {wildcards.insrate} --indel-size-zipf {wildcards.idzipf} --nwk wgd_tree.nwk > {output.task} 2> {log}'

rule simulate_natural:
    input:
        'simulate'
    output:
        task='genomes/natural/simulated/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/sample{n}.txt',
    log:
        'genomes/natural/simulated/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/sample{n}.log'
    shell:
        './simulate --genes {wildcards.markers} --nops {wildcards.ops} --nchrms {wildcards.linears} --dup-rate {wildcards.duprate} --dup-size-zipf {wildcards.dupzipf} --del-rate {wildcards.delrate} --ins-rate {wildcards.insrate} --indel-size-zipf {wildcards.idzipf} --nwk wgd_tree.nwk > {output.task} 2> {log}'

rule get_uq:
    input:
        'genomes/{natsing}/simulated/{params}/{gnm}.log'
    output:
        temp('genomes/{natsing}/simulated/{params}/{gnm}.uq')
    shell:
        'cat {input} | grep ">G" -A 1000000000 > {output}'

rule get_true:
    input:
        gnm='genomes/{natsing}/simulated/{params}/{gnm}.txt',
        uq='genomes/{natsing}/simulated/{params}/{gnm}.uq',
        log='genomes/{natsing}/simulated/{params}/{gnm}.log'
    output:
        'genomes/singular/simulated_truth_from_{natsing}/{params}/{gnm}.txt'
    shell:
        './find_true_matching.py {input.gnm} {input.log} {input.uq} > {output}'

rule compeau_halve:
    input:
        'genomes/singular/{subdir}/{name}.txt'
    output:
        gnm='pairs/compeau/{subdir}/{name}.txt',
        dist='dist_sg/compeau/{subdir}/{name}.txt'
    log:
        'dist_sg/compeau/{subdir}/{name}.log'
    shell:
        './halving_compeau.py {input} --writegenometo {output.gnm} -o -u 2> {log} > {output.dist}'

rule unimog_check:
    input:
        'pairs/{path}/{name}.txt'
    output:
        'dist_sg/unimog/{path}/{name}.txt'
    shell:
        '%s {input} > {output}'%UNIMOG
        
        
rule create_ilp:
    input:
        'genomes/{type}/simulated/{path}/{name}.txt'
    output:
        temp('ilp/{ilpstr}/{type}/{path}/{name}.lp')
    run:
        entries = wildcards.ilpstr.split('_')
        ilpname = entries[0]
        params = ' '.join([MOD_MAP[c] for c in entries[1]])
        if ilpname == 'journal':
            shell('./hang.py {input} --outfile {output} %s'%(params))
        else:
            shell('./generate_ilps.py %s {input} {output} %s'%(ilpname,params))

rule solve_ilp:
    input:
        'ilp/{subpath}/{name}.lp'
    output:
        'sol/{subpath}/{name}.sol'
    log:
        'sol/{subpath}/{name}.log'
    threads:
        ILP_THREADS
    shell:
        './solve_gurobi.py -t {threads} --timelim %s {input} {output} > {log}'%(ILP_TIME)

rule all_solution:
    input:
        expand('sol/{ilpname}/%s/{simulation}/sample{n}.sol'%MODE,ilpname=ALL_ILPS,simulation=ALL_SIMULATIONS,n=range(1,NUM_SAMPLES+1))

rule ilp_dist:
    input:
        sol='sol/{ilpname}/{type}/{path}/{name}.sol',
        template='genomes/{type}/simulated/{path}/{name}.txt'
    output:
        gnm='genomes/singular/{ilpname}/from_{type}/{path}/{name}.txt',
        dist='dist/{ilpname}/{type}/{path}/{name}.txt',
    log:
        'genomes/singular/{ilpname}/from_{type}/{path}/{name}.log'
    run:
        entries = wildcards.ilpname.split('_')
        ilpname = entries[0]
        params = ' '.join([MOD_MAP[c] for c in entries[1]])
        if ilpname == 'journal':
            shell('./hang_dist.py {input.sol} {input.template} --matching {output.gnm} > {output.dist}')
        else:
            shell('./interpret_sol.py {input.sol} --writematching {output.gnm} --template-genome {input.template} > {output.dist}')

rule summarize_singular:
    input:
        simlog='genomes/singular/simulated/{params}/{name}.log',
        compeau='dist_sg/compeau/simulated/{params}/{name}.txt',
        comp_log='dist_sg/compeau/simulated/{params}/{name}.log',
        unimog='dist_sg/unimog/compeau/simulated/{params}/{name}.txt',
        ilps=expand('dist/{ilpname}/singular/{{params}}/{{name}}.txt',ilpname=ALL_ILPS)
    output:
        'summary/singular/{params}/{name}.txt'
    run:
        ilpstring = ','.join(['$(grep ":" %s | cut -f 2 -d ":" | tr -d " ")'%ilpname for ilpname in input.ilps])
        shell('echo  "{wildcards.params}{wildcards.name}",$(grep -o "distance of.*operations" {input.simlog} | cut -f 3 -d " "),$(cat {input.compeau} | cut -f 2 -d ":" | tr -d " "), $(grep "Operation" {input.comp_log} | wc -l), $(grep "=" {input.unimog} | cut -f 2 -d "=" | tr -d "\r" | tr -d " "), %s > {output}'%ilpstring)

rule header_singular:
    output:
        'singular_header.txt'
    run:
        ilpstring = ','.join(['%s_dist'%ilpname for ilpname in ALL_ILPS])
        shell('echo filename,simulated_dist,compeau_calc_dist,compeau_sort_dist,unimog_dist,%s > {output}'%ilpstring)

rule summarize_natural:
    input:
        compeau='dist_sg/compeau/naive_/from_natural/{params}/{name}.txt',
        comp_log='dist_sg/compeau/naive_/from_natural/{params}/{name}.log',
        unimog='dist_sg/unimog/compeau/naive_/from_natural/{params}/{name}.txt',
        ilps=expand('dist/{ilpname}/natural/{{params}}/{{name}}.txt',ilpname=ALL_ILPS)
    output:
        'summary/natural/{params}/{name}.txt'
    run:
        ilpstring = ','.join(['$(grep ":" %s | cut -f 2 -d ":" | tr -d " ")'%ilpname for ilpname in input.ilps])
        shell('echo  "{wildcards.params}{wildcards.name}",$(cat {input.compeau} | cut -f 2 -d ":" | tr -d " "),$(grep "Operation" {input.comp_log} | wc -l),$(grep "=" {input.unimog} | cut -f 2 -d "=" | tr -d "\r" | tr -d " "), %s > {output}'%ilpstring)

rule all_performance_stat:
    input:
        expand('performance/%s/{simulation}/sample{n}.txt'%MODE,simulation=ALL_SIMULATIONS,n=range(1,NUM_SAMPLES+1))
    output:
        'performance/%s/summary.txt'%MODE
    shell:
        'cat {input} > {output}'
        
        
rule performance_stat:
    input:
        ilpdist='dist/{ilpname}/%s/{params}/{name}.txt'%MODE,
        ilplog ='sol/{ilpname}/%s/{params}/{name}.log'%MODE,
        ilpsim='similarity/{ilpname}/from_%s/{params}/{name}.txt'%MODE
    output:
        temp('performance/ilp/{ilpname}/%s/{params}/{name}.txt'%MODE)
    shell:
        'echo $(cut -f 2 -d ":" {input.ilpdist} | tr -d " "),$(cat {input.ilpsim}),$(grep -oP "(?<=in ).*(?=seconds)" {input.ilplog} | tr -d "\n "),$(grep -oP "(?<=gap ).*" {input.ilplog} | tr -d "\n ") > {output}'

rule genome_stat:
    input:
        simlog='genomes/%s/simulated/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/{name}.log'%MODE,
        simgnm='genomes/%s/simulated/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/{name}.txt'%MODE
    output:
        temp('performance/genome/%s/ops{ops}_markers{markers}_linears{linears}_duprate{duprate}_dupzipf{dupzipf}_delrate{delrate}_insrate{insrate}_idzipf{idzipf}/{name}.txt'%MODE)
    shell:
        'echo {wildcards.duprate},{wildcards.dupzipf},{wildcards.delrate},{wildcards.insrate},{wildcards.idzipf},$(./genome_stats.py {input.simgnm}),{wildcards.ops} > {output}'

rule matching_similarity:
    input:
        reference='genomes/singular/simulated_truth_from_{type}/{path}/{name}.txt',
        matching='genomes/singular/{ilpname}/from_{type}/{path}/{name}.txt'
    output:
        temp('similarity/{ilpname}/from_{type}/{path}/{name}.txt')
    shell:
        './test_matching_overlap.py {input.reference} {input.matching} > {output}'
        

rule performance_statblock:
    input:
        gnmblock='performance/genome/%s/{params}/{name}.txt'%MODE,
        ilpblocks=expand('performance/ilp/{ilpname}/%s/{{params}}/{{name}}.txt'%MODE, ilpname=ALL_ILPS)
    output:
        temp('performance/%s/{params}/{name}.txt'%MODE)
    shell:
        'paste {input.gnmblock} {input.ilpblocks} -d "," > {output}'

rule header_statblock:
    output:
        'performance/%s/header.txt'%MODE
    run:
        #l = ['theta','delrate','idlenlow','idlenhigh','genome_size','ambiguous','unbalanced','max_famsize','median_famsize','lin_chr','simulated_distance']
        l = ['duprate','dupzipf','delrate','insrate','idzipf','genome_size','ambiguous','unbalanced','max_famsize','median_famsize','lin_chr','sim_dist']
        for ilp in ALL_ILPS:
            l.append('%s_distance'%ilp)
            l.append('%s_jaccard'%ilp)
            l.append('%s_runtime[s]'%ilp)
            l.append('%s_gap'%ilp)
        header = ','.join(['%d_%s'%(i,n) for i,n in enumerate(l)])
        shell('echo %s > {output}'%header)
