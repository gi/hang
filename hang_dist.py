#!/usr/bin/python3

from ilp_util import *
from argparse import ArgumentParser
import argparse as ap
import sys
from sol_util import *
import math



def set_edges(sng,idxe,vars):
    eidx = {v:k for k,v in idxe.items()}
    for e,v in vars['x'].items():
        x,y,k = eidx[int(e)]
        if not v > 0:
            sng.remove_edge(x,y,k)
    

def is_cycle(component,checkeven=False):
    comp = list(nx.eulerian_path(component))
    if len(comp)==0:
        return False
    if component.degree[comp[0][0]] == 2:
        if checkeven:
            if (len(comp)/2)%2 == 0:
                return True,True
            else:
                return True,False
        else:
            return True
    return False

ANNOT = 'annot'
def annotate_vertices(sng):
    for v in sng.nodes():
        if sng.degree[v] <= 1:
            edges = [(v,u,k) for u in sng[v] for k in sng[v][u]]
            if len(edges) > 1:
                raise AssertionError("Wrong number of edges ({}) for degree 1 at vertex {}".format(len(edges),v))
            if len(edges) == 0:
                sng.nodes[v][ANNOT] = 'ti'
            else:
                x,y,k = edges[0]
                tp = 't' if sng[x][y][k]['etype'] == EXTREMITYEDGE else 'i'
                sng.nodes[v][ANNOT]=tp


def sanity_check(sng,idxv):
    #count circular singletons
    circsings = 0
    for component in [sng.subgraph(c) for c in nx.connected_components(sng)]:
        
        comp = set([sng[x][y][k]['etype'] for x,y,k in nx.eulerian_path(component,keys=True)])
        if is_cycle(component) and EXTREMITYEDGE not in comp:
            circsings+=1
    ides = [(x,y,k) for x,y,k,etype in sng.edges(keys=True,data='etype') if etype==SELFEDGE]
    for x,y,k in ides:
        sng.remove_edge(x,y,k)
    annotate_vertices(sng)
    compcounts = {'singleton': circsings,'n':0.5*len([1 for x,y,k in sng.edges(keys=True) if sng[x][y][k]['etype'] == EXTREMITYEDGE])}
    for component in [sng.subgraph(c) for c in nx.connected_components(sng)]:
        epath = list(nx.eulerian_path(component,keys=True))
        isodd = ((len([1 for x,y,k in epath if sng[x][y][k]['etype']==EXTREMITYEDGE])%2)==1)
        parity = 's' if isodd else 'o'
        if is_cycle(component):
            comptype = 'c'+parity
        else:
            annots = [a for x in component.nodes() for a in sng.nodes[x].get(ANNOT,'')]
            if len(annots) != 2:
                print("WARNING: Weird annotation: {}".format(annots))
            raw = ''.join(sorted(annots))
            comptype = raw[1]+parity+raw[0]
        if comptype not in compcounts:
            compcounts[comptype] = 0
        if 't' in comptype:
            print(comptype,end=' : ')
            if len(list(component.nodes()))==1:
                print([idxv[v] for v in component.nodes()])
            else:
                print([idxv[v] for v,_,_ in epath]+[idxv[epath[-1][1]]])
        compcounts[comptype]+=1
    print(compcounts)
    


def main():
    parser = ArgumentParser("Generate a capping-free ILP to halve natural Genomes.")
    parser.add_argument("solfile",help="Gurobi Solution file.")
    parser.add_argument("genome",help="Unimog Genome file of the unlabeled genome")
    parser.add_argument("--matching",type=ap.FileType('w'),default=sys.stdout)
    args = parser.parse_args()
    gnms = readGenomes(args.genome)
    if len(gnms) < 1:
        LOG.info('No genomes in file! Exiting...')
        return
    if len(gnms) > 1:
        LOG.warning('Will ignore other genomes except first.')
    gnm = gnms[0]
    chrms = gnm[1]
    sng, _, _ = build_graph(gnm,closing_style='bws')
    obj, vars = read_variables(args.solfile, rounding=False)
    is_capped =  not 'a' in vars
    create_capping(sng,light=not is_capped)
    idxv = generate_vertex_indices(sng,start_index=1,caps_low=not is_capped)
    idxe = generate_edge_indices(sng,start_index=1)
    print('Halving distance: %d'%(int(math.ceil(obj))))
    #print(is_capped,file=sys.stderr)
    #print(vars['a'],file=sys.stderr)
    set_edges(sng=sng,idxe=idxe,vars=vars)
    remove_capping(sng)
    md = create_matching(sng)
    for c in chrms:
        for m in c[1]:
            m.gene = md[m.mid]
    print('>%s'%gnm[0],file=args.matching)
    print_chromosomes(chrms,marker_ids=False,file=args.matching)
    #sanity_check(sng,idxv)

main()