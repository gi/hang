#! /usr/bin/python3
from argparse import ArgumentParser
from ilp_util import readGenomes,CHR_LINEAR
from halving_util import *
import statistics as s

def main():
    parser = ArgumentParser('Calculate different statistics of a genome and output as a csv-line, namely: genome size, #ambiguous families, #odd families, max. fam. size, median fam. size, #telomeres')
    parser.add_argument('genome')
    args = parser.parse_args()
    chroms = readGenomes(args.genome)[0][1]
    telos = 0
    famcounts = {}
    for ctype, chrom in chroms:
        if ctype == CHR_LINEAR:
            telos += 1
        for marker in chrom:
            if not marker.gene in famcounts:
                famcounts[marker.gene] = 0
            famcounts[marker.gene]+=1
    unbalanced = 0
    ambiguous = 0
    fcs = [x for _,x in famcounts.items()]
    gsize = sum([len(ms) for _,ms in chroms])
    for fam, count in famcounts.items():
        if count >= 3:
            ambiguous+=1
        if count %2 == 1:
            unbalanced +=1
    print('%d,%d,%d,%d,%d,%d'%(gsize,ambiguous,unbalanced,max(fcs),s.median(fcs),telos))

main()
