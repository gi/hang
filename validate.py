#!/usr/bin/python3
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('file')
parser.add_argument('-i',action='store_true',help='Ignore missing values')
parser.add_argument('--header',type=str)
args = parser.parse_args()

def rename(i,names=[]):
    if len(names)<i:
        return i
    else:
        return names[i]

with open(args.file) as f:
    broken_rows = []
    all_ok = True
    for line in f:
        entries = [e.strip() for e in line.split(',')]
        nums = []
        this_row_ok = True
        for e in entries[1:]:
            try:
                n = int(e)
                nums.append(n)
            except ValueError:
                if args.i:
                    continue
                else:
                    all_ok=False
                    print('Missing value in row/could not parse "{}"'.format(e))
        for i in nums[1:]:
            if i > nums[0]:
                this_row_ok=False
                all_ok=False
                print('Larger distance than simulated: {}'.format(entries))
            for j in nums[1:]:
                if i != j:
                    this_row_ok=False
        if not this_row_ok:
            broken_rows.append((line,nums[1:]))
            print('Problem with row: %s'%line)
    if len(broken_rows) == 0 and all_ok:
        print('All numbers match :)')
    else:
        print('There were errors.')
        names = []
        if args.header:
            with open(args.header) as g:
                names=g.readline().split(',')
        base_group = set([i for i,_ in enumerate(broken_rows[0][1])])
        agreeing_groups = [base_group]
        for line,nums in broken_rows:
            val_to_i = dict()
            for i,n in enumerate(nums):
                if not n in val_to_i:
                    val_to_i[n] = set()
                val_to_i[n].add(i)
            new_groups= []
            for group in agreeing_groups:
                for valgroup in val_to_i.values():
                    g = valgroup.intersection(group)
                    if len(g) > 0:
                        new_groups.append(g)
                        if len(g) < len(group)/2:
                            print('Line {} splits {} from {}'.format(line,[rename(i+2,names=names) for i in g],[rename(i+2,names=names) for i in group]))
            agreeing_groups = new_groups
        print('Results behave in the following groups:')
        
        for group in agreeing_groups:
            print([rename(i+2,names=names) for i in group])
