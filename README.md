# Halving Natural Genomes

A snakemake workflow for performance testing ILPs designed to halve natural genomes.

The two scripts implementing the hang-ILP:

<details><summary>hang.py</summary>
Generate the hang ILP.

Parameter overview:

| parameter | function |
| ------ | ------ |
| `input` | Input genome in `unimog` format. **Warning**: marker identifiers cannot contain character `_`. |
|` --outfile`| Output ILP in `gurobi` format. |
|`--matching-model k`| Choose `k`=`mm` for maximal matching, `k`=`im` for intermediate matching and `em` for exemplary matching, not available for `k`=`naive`. |
| `--capping`  | Force a capping of the graph and drop some variables and constraints. Only for testing purposes, not recommended. |
</details>

<details><summary>interpret_sol.py</summary>
Extract the distance and matching from a `gurobi` solution file.

Parameter overview:

| parameter | function |
| ------ | ------ |
| `solfile` | `gurobi` solution. |
|`genome`| The original unmatched genome in `unimog` format.  |
| `--matching M` | Write the resulting matched genome to file `M` |
</details>

## Overview of the workflow

The performance testing of the workflow can be performed using the rule `all_performance_stat` (run `snakemake all_performance_stat` in a terminal). Its results are stored as a csv in `peformance/natural/summary.txt`.

For a header to this csv file run `snakemake header_statblock`. The header file can be found as `performance/natural/header.txt`.

### Overview of the config file

The config file is stored as `config.yaml`.

| parameter | function |
| ------ | ------ |
| `ops` | The number of operations to be performed by the simulation |
| `markers` | The number of markers at the root of the simulation |
| `linears` | The number of linear chromosomes at the root of the simulation |
| `insrate` | The relative (w.r.t. DCJs) rate with which hat insertions are performed |
| `delrate` | The relative rate with which deletions are performed |
| `duprate`| The relative rate with which duplications are performed |
| `idzipf` | Indel size distribution Zipf parameter |
| `dupzipf` | Duplication size distribution Zipf parameter |
| `groups` | List of groups of parameters to be linked |
| `num_samples` | The number of samples to create per parameter combination.  |
| `ìlp_threads` | The number of threads to use per gurobi run |
| `ilp_time` | The time limit for gurobi in seconds |
| `unimog_cmd` | If the user wishes to validate halving distances computed, the call for the Unimog |

The parameter `groups` allows the user to link different parameters. For example, with no `groups` set:
`groups : []`
the example config file simulates all combinations of number of markers and chromosomes, duplication and deletion rates.

Setting the parameter like so:
`groups : [['markers','linears'],['delrate','duprate']]`
The number markers is now coupled with the number of linear chromosomes and the deletion rate is coupled with the duplication rate, meaning only 6 combinations of parameters are tested (instead of the 3*3*2*2 = 36 without groups), namely:
* markers = 1000 linears: 1 delrate = 1.0 duprate: 0.3 
* markers = 1000 linears: 1 delrate = 2.0 duprate: 0.4
* markers = 2000 linears: 2 delrate = 1.0 duprate: 0.3 
* markers = 2000 linears: 2 delrate = 2.0 duprate: 0.4
* markers = 3000 linears: 3 delrate = 1.0 duprate: 0.3 
* markers = 3000 linears: 3 delrate = 2.0 duprate: 0.4


The parameters `mode` and`ilps` typicaly do not have to be changed by the user. 



