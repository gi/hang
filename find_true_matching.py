#!/usr/bin/python3

from ilp_util import readGenomes,print_chromosomes
from argparse import ArgumentParser

def wgd_matches_from_logfile(lf):
    matches = {}
    reached_wgd = False
    keyword = "Inserting "
    with open(lf) as f:
        for line in f:
            if line.startswith("Performing WGD."):
                reached_wgd = True
                continue
            if reached_wgd:
                if line.startswith("Finished simulating branch"):
                    return matches
                if keyword in line and "as" in line:
                    line = line[len(keyword)::]
                    entries = line.split(" as ")
                    a = entries[0].strip()
                    b = entries[1].strip()
                    if a.startswith('-'):
                        a=a[1::]
                    if b.startswith('-'):
                        b=b[1::]
                    matches[a]=b
                    matches[b]=a
    return matches
                



def main():
    parser = ArgumentParser()
    parser.add_argument('genomefile')
    parser.add_argument('logfile')
    parser.add_argument('uqlabeling')
    args = parser.parse_args()
    filegnm = readGenomes(args.genomefile)[0]
    uqgnm = readGenomes(args.uqlabeling)[0]
    matches = wgd_matches_from_logfile(args.logfile)
    #print(matches)
    n_seen = {}
    names = {}
    for c1,c2 in zip(filegnm[1],uqgnm[1]):
        for m1,m2 in zip(c1[1],c2[1]):
            partner_seen = False
            if m2.gene in matches:
                partner_seen = matches[m2.gene] in names
            if partner_seen:
                m1.gene = names[matches[m2.gene]]
            else:
                if not m1.gene in n_seen:
                    n_seen[m1.gene] = 0
                n_seen[m1.gene]+=1
                names[m2.gene] = '%s_%d'%(m1.gene,n_seen[m1.gene])
                m1.gene = names[m2.gene]
    print(">"+filegnm[0])
    print_chromosomes(filegnm[1])


main()


