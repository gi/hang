#!/usr/bin/python3
from ilp_util import readGenomes
from halving_util import print_chromosomes
from argparse import ArgumentParser


def find_families(chrs):
	fams = {}
	for _,ms in chrs:
		for m in ms:
			if not m.gene in fams:
				fams[m.gene] =[]
			fams[m.gene].append(m.mid)
	return fams 

def fams_to_matching(fams):
	match = {}
	for f,mids in fams.items():
		if len(mids) > 2:
			raise AssertionError('Not a matching! Family: {}'.format(f))
		if len(mids) ==2:
			match[mids[0]] = mids[1]
			match[mids[1]] = mids[0]
		else:
			match[mids[0]] = None
	return match

def get_translator(chrs1,chrs2):
	match = {None:None}
	for oc1,oc2 in zip(chrs1,chrs2):
		_,c1 = oc1
		_,c2 = oc2
		for m1, m2 in zip(c1,c2):
			match[m1.mid] = m2.mid
	return match

def main():
	parser = ArgumentParser('Calculate the Jaccard-similarity of two matchings. All chromosomes must be notated in the same order, direction and beginning with the same marker. ')
	parser.add_argument('genome1')
	parser.add_argument('genome2')
	args = parser.parse_args()
	chrs1 = readGenomes(args.genome1)[0][1]
	chrs2 = readGenomes(args.genome2)[0][1]
	mtch1 = fams_to_matching(find_families(chrs1))
	mtch2 = fams_to_matching(find_families(chrs2))
	pmtch = get_translator(chrs2,chrs1)
	commons = 0
	matchings2 = set()
	for m1,m2 in mtch2.items():
		mm1 = pmtch[m1]
		mm2 = pmtch[m2]
		matchings2.add(tuple([mm1,mm2]))
	matchings1 = set()
	for mm1,mm2 in mtch1.items():
		matchings1.add(tuple([mm1,mm2]))
	jaccard_sim = len(matchings1.intersection(matchings2))/(len(matchings1.union(matchings2)))
	print(jaccard_sim)


main()
