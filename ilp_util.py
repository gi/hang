from halving_util import *
import networkx as nx
import functools

ID_GEN = Simple_Id_Generator()

SUBID_SEP = '.'

SEPARATOR = '_'
S=SEPARATOR
ESEP = '__'
CAP = 'CAP'
REGULAR = 'REG'
COMPLETION='COM'
def makemarker(s, fams):
    '''
    Create a marker from an unimog-marker entry, while extending the family dictionary.
    '''
    s = s.strip()
    orient = ORIENT_POSITIVE
    if s.startswith(ORIENT_NEGATIVE):
        orient = ORIENT_NEGATIVE
        s = s[1:].strip()
    elif s.startswith(ORIENT_POSITIVE):
        s = s[1:].strip()
    if not s in fams:
        fams[s] = []
    fams[s].append(s)
    return Marker(s,'{}{}{}'.format(s,SUBID_SEP,len(fams[s])),direction = orient)

def readGenomes(fl):
    '''
    Read multiple genomes from an unimog-file.
    '''
    gen_name = ''
    chromosomes = []
    genomes = []
    at_least_one = False
    fams = {}
    with open(fl) as f:
        for line in f:
            line = line.strip()
            if line == '':
                continue
            if line.startswith('>'):
                genomes.append((gen_name, chromosomes))
                gen_name = line[1:].strip()
                chromosomes = []
                at_least_one = True
                continue
            if line[-1] not in [CHR_LINEAR, CHR_CIRCULAR]:
                raise AssertionError('%s not a recognized chromosome type (%s for linear or %s for circular).'%(line[-1], CHR_LINEAR, CHR_CIRCULAR))
            chromosomes.append((line[-1],[makemarker(s,fams) for s in line[:-1].split()]))
    if at_least_one:
        genomes = genomes[1:]
        genomes.append((gen_name, chromosomes))
    return genomes


def build_graph(genome,only_sing_dup=False, closing_style='compeau'):
    '''
    Create the supernatural diagram for a genome.
    
    genome: the genome for which to construct the graph
    only_sing_dup: check whether the genome is partially duplicated
    closing_style :{'compeau','bws'} switch between Compeau and BWS styles of closing
    '''
    name, chromosomes = genome
    adj = get_adjacencies(chromosomes)
    sng = nx.MultiGraph()
    sng.add_nodes_from(adj.keys(),ntype=REGULAR)
    completion_extremities = []
    marker_dict = {}
    for x,y in adj.items():
        if y[1] == EXTREMITY_TELOMERE:
            continue
        if not sng.has_edge(x,y):
            sng.add_edge(x,y, etype=ADJACENCY)
    fams = get_familiy_dict(chromosomes)
    for f, ms in fams.items():
        ms_ = ms
        if only_sing_dup and len(ms_) > 2:
            raise AssertionError('Family {} has > 2 Members ({}).'.format(f,len(ms)))
            
        if closing_style=='bws':
            for m in ms:
                    x,y = m.get_extremities(directionless=True)
                    sng.add_edge(x,y, etype=SELFEDGE)
        if len(ms_)%2 == 1:
            if closing_style=='compeau':
                c_marker = Marker(f,'{}{}{}'.format(f,SUBID_SEP,len(ms_)+1))
                ms_.append(c_marker)
                completion_extremities.extend(c_marker.get_extremities())
                sng.add_nodes_from(c_marker.get_extremities(),ntype=COMPLETION)
            elif not closing_style=='bws':
                raise AssertionError('No valid closing style: {}'.format(closing_style))
        for i in range(len(ms_)):
            for j in range(i+1,len(ms_)):
                a = ms_[i].get_extremities(directionless=True)
                b = ms_[j].get_extremities(directionless=True)
                marker_dict[ms_[i].mid] = ms_[i].gene
                marker_dict[ms_[j].mid] = ms_[j].gene
                sng.add_edge(a[0],b[0],etype=EXTREMITYEDGE)
                sng.add_edge(a[1],b[1],etype=EXTREMITYEDGE)
    return sng, completion_extremities, marker_dict
    

def add_all_possible_completions(sng,ces):
    '''
    Add all possible adjacencies in a Compeau-style closed supernatural diagram.
    '''
    for i in range(len(ces)):
        for j in range(i+1,len(ces)):
            x = ces[i]
            y = ces[j]
            sng.add_edge(x,y,etype=ADJACENCY_C)

def print_chromosomes_from_graph(sng,marker_dict=None,marker_ids=False,file=sys.stdout,normalize_off=False):
    '''
    Print the chromosomes of the genome the supernatural diagram describes.
    
    marker_dict: dictionary mapping marker ids to their families
    
    marker_ids: use marker ids instead of families for canonization and printing
    
    file: file to print to
    
    normalize_off: do not canonize the representation
    
    '''
    gnm  = DCJ_Genome(adjacencies_from_graph(sng))
    chrs = gnm.reconstruct_genome(marker_dict=marker_dict)
    if not normalize_off:
        chrs = normalize_chromosomes(chrs,marker_ids=marker_ids)
    print_chromosomes(chrs,marker_ids=marker_ids,file=file)

def get_telomeres(sng):
    telomeres = []
    for node in sng.nodes:
        if not functools.reduce(lambda a,b : a or b, [data['etype'] == ADJACENCY or data['etype'] == ADJACENCY_C for _,_,data in sng.edges(node,data=True)]):
            telomeres.append(node)
    return telomeres

            
def create_capping(sng,light=False):
    '''
    Add capping to the supernatural diagram.
    light: only perform a pseudo-capping without extremity edges.
    '''
    caps = []
    tels = get_telomeres(sng)
    for tel in tels:
        cap = ('{}{}{}'.format(CAP_ID,SUBID_SEP,len(caps)+1),EXTREMITY_TELOMERE)
        caps.append(cap)
        sng.add_node(cap,ntype=CAP)
        sng.add_edge(tel,cap, etype=ADJACENCY)
    if light:
        return
    if (len(tels)//2)%2 == 1:
        cap1= ('{}{}{}'.format(CAP_ID,SUBID_SEP,len(caps)+1),EXTREMITY_TELOMERE)
        caps.append(cap1)
        sng.add_node(cap1,ntype=CAP)
        cap2 = ('{}{}{}'.format(CAP_ID,SUBID_SEP,len(caps)+1),EXTREMITY_TELOMERE)
        caps.append(cap2)
        sng.add_node(cap2,ntype=CAP)
        sng.add_edge(cap1,cap2,etype=ADJACENCY)
    for i in range(len(caps)):
        for j in range(i+1,len(caps)):
            sng.add_edge(caps[i],caps[j],etype=EXTREMITYEDGE)

def adjacencies_from_graph(sng):
    '''
    Obtain the collection of adjacencies from the supernatural diagram.
    '''
    adj = {}
    for x,y in [(k,l) for k,l, data in sng.edges(data=True) if data['etype']==ADJACENCY]:
        adj[y]=x
        adj[x]=y
    for x in sng.nodes():
        if x not in adj:
            adj[x] = (None,EXTREMITY_TELOMERE)
    return adj

def get_sibling_edges(sng):
    '''
    Obtain all sibling edge pairs from the supernatural diagram.
    '''
    sibs = []
    for xt,yt,key in [(x,y,key) for x,y,key,etype in sng.edges(keys=True,data='etype') if etype==EXTREMITYEDGE and x[1]==EXTREMITY_TAIL]:
        xh = sibling_extremity(xt)
        yh = sibling_extremity(yt)
        mby_sibkey = [i for i in sng[xh][yh] if sng[xh][yh][i]['etype'] ==EXTREMITYEDGE ]
        if len(mby_sibkey) != 1:
            raise AssertionError('Edge {}{} has {} siblings!'.format(xt,yt,len(mby_sibkey)))
        sibkey = mby_sibkey[0]
        sibs.append(((xt,yt,key),(xh,yh,sibkey)))
    return sibs

def cdx(xtr):
    '''
    Canonical display for an extremity.
    '''
    m,x = xtr
    return SEPARATOR.join([str(m),str(x)])

def cde(e,etype):
    '''
    Canonical display for an edge.
    '''
    xtr1,xtr2,key= e
    xtr1,xtr2 = tuple(sorted((xtr1,xtr2)))
    return ESEP.join([cdx(xtr1),cdx(xtr2) , str(key),str(etype)])

def generate_vertex_indices(sng,start_index=1,caps_low=False):
    '''
    Create the index mapping i(u) for all vertices.
    '''
    idx = {}
    if caps_low:
        for v,ntype in sng.nodes(data="ntype"):
            if ntype==CAP:
                idx[v] = len(idx)+start_index
        for v,ntype in sng.nodes(data="ntype"):
            if ntype!=CAP:
                idx[v] = len(idx)+start_index
    else:
        for v in sng.nodes:
            idx[v] = len(idx)+start_index
    return idx

def generate_edge_indices(sng,start_index=1):
    idx = {}
    for x,y,key,etype in sng.edges(keys = True, data='etype'):
        eid = len(idx)+start_index
        idx[(x,y,key)] = eid
        idx[(y,x,key)] = eid
    return idx