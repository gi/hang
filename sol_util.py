import networkx as nx
from ilp_util import *

def read_variables(file, rounding=True):
    '''
    Read a gurobi solution and extract all variable values.
    
    file: the gurobi solution file
    rounding: round the float values in the solution
    '''
    obj = None
    var = {}
    with open(file) as f:
        for line in f:
            if line.startswith('#'):
                if 'Objective value =' in line:                   
                    o = float(line.split('=')[1].strip())
                    if rounding:
                        o = int(round(o))
                    if not obj == None and obj != o:
                        raise AssertionError('Two objective values given!')
                    obj = o
                else:
                    print('Comment: {}'.format(line.strip()))
                continue
            entries = line.strip().split()
            if (entries[0][1] if len(entries[0]) > 1 else '') == SEPARATOR:
                prefix = entries[0][0]
                suffix = entries[0][2:]
            else:
                prefix = entries[0]
                suffix = ''
            #print(entries,prefix,suffix)
            if not prefix in var:
                var[prefix] = {}
            if suffix in var[prefix]:
                raise AssertionError('Double assignment of variable %s'%entries[0])
            
            var[prefix][suffix] = float(entries[1])
            if rounding:
                var[prefix][suffix] = int(round(var[prefix][suffix]))
        return obj, var
        
def parse_extremity(un):
    return tuple(un.split(SEPARATOR))
        
def build_graph_from_vars(var,rounding=False):
    '''
    Build the decomposition based on the included edges as decided by the x-variables.
    '''
    sng = nx.MultiGraph()
    for edgename in var['x']:
        if (var['x'][edgename] < 1 and not rounding) or round(var['x'][edgename]) < 1:
            continue
        un,vn,keyn,etype = edgename.split(ESEP)
        u = parse_extremity(un)
        v = parse_extremity(vn)
        if not sng.has_node(u):
            sng.add_node(u)
        if not sng.has_node(v):
            sng.add_node(v)
        sng.add_edge(u,v,etype=etype)
    return sng

def create_matching(sng):
    '''
    Get the matching implied by the decomposition.
    '''
    md = {}
    fams = {}
    for u,v in [(u,v) for u,v, etype in sng.edges(data='etype') if etype==EXTREMITYEDGE and u[1]==EXTREMITY_HEAD]:
        f = u[0].split(SUBID_SEP)[0]
        if not f in fams:
            fams[f] = 0
        fams[f]+=1
        newfam = '{}{}{}'.format(f,SUBID_SEP,fams[f])
        md[u[0]] = newfam
        md[v[0]] = newfam
    delid = 0
    for u,v in [(u,v) for u,v, etype in sng.edges(data='etype') if etype==SELFEDGE]:
        delid+=1
        f = u[0].split(SUBID_SEP)[0]
        md[u[0]] = '{}{}x{}'.format(f,SUBID_SEP,delid)
    return md

def remove_capping(sng):
    for v in [v for v in sng.nodes if v[1] == EXTREMITY_TELOMERE]:
        sng.remove_node(v)
